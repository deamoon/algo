//http://acm.timus.ru/problem.aspx?space=1&num=1022
#include <iostream>
#include <stack>
#include <stdio.h>

using namespace std;

int N, A[101][101], B[101];
stack<int> s;

void DFS(int n) {
    int i;
    if (B[n] == 2) return; // Если черная 
    B[n] = 1; // Серая
    for (int i = 1; i <= N; ++i) {
    	if ((A[n][i] == 1) && (B[i] == 0)) {
    		DFS(i);
    	} 
    }	
    s.push(n); 
    B[n] = 2; // Черная
}

int main(int argc, char const *argv[]) {
	int a;
    freopen("1.txt", "r", stdin);
	for (int i = 1; i <= 100; ++i) {
		for (int j = 1; j <= 100; ++j) {
			A[i][j] = 0;
		}
		B[i] = 0; // Белая
	}	
			
	cin>>N;
	for (int i = 1; i <= N; ++i) {
		while (cin>>a) {
            if (a==0) break;
            A[i][a] = 1;
		}
	}
	for (int i = 1; i <= N; ++i) {
 		DFS(i);
	}	

    while (!s.empty()) {
    	cout<<s.top()<<' ';
    	s.pop();
    }	
    cout<<endl;

	return 0;
}